package com.globalbit.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ahmadrosid.svgloader.SvgLoader;
import com.globalbit.R;
import com.globalbit.model.Countries;
import java.util.ArrayList;
import java.util.Objects;

public class RecyclerAdapterActivityBorders extends RecyclerView.Adapter<RecyclerAdapterActivityBorders.ListBordersViewHolder> {
    private Context mContext;
    private ArrayList<Countries> mCountriesList;
    private Activity mActivity;

    public RecyclerAdapterActivityBorders(Context context, ArrayList<Countries> countriesList, Activity activity) {
        mContext = context;
        mCountriesList = countriesList;
        mActivity = activity;
    }

    @NonNull
    @Override
    public RecyclerAdapterActivityBorders.ListBordersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.borders_list_item, viewGroup, false);
        ListBordersViewHolder viewHolder = new ListBordersViewHolder(view);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterActivityBorders.ListBordersViewHolder listBordersViewHolder, int i) {
        listBordersViewHolder.bindList(mCountriesList.get(i));

    }

    @Override
    public int getItemCount() {
        if (mCountriesList != null) {
            return mCountriesList.size();
        } else {
            return 0;
        }
    }

    public class ListBordersViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextViewCountryName;
        public TextView mTextViewCapital;
        public TextView mTextViewNativeName;
        public TextView mTextViewBorders;
        public ImageView mImageViewFlag;

        public ListBordersViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextViewCountryName = itemView.findViewById(R.id.textViewName);
            mTextViewCapital = itemView.findViewById(R.id.textViewCapital);
            mTextViewNativeName = itemView.findViewById(R.id.textViewNativeName);
            mTextViewBorders = itemView.findViewById(R.id.textViewBorders);
            mImageViewFlag = itemView.findViewById(R.id.imageViewFlag);

        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public void bindList(Countries countries) {

            SvgLoader.pluck()
                    .with(mActivity)
                    .load(Objects.requireNonNull(countries).getFlag(), mImageViewFlag);

            mTextViewCountryName.setText(mContext.getResources().getString(R.string.latin_name) + " " + countries.getName());
            mTextViewNativeName.setText(mContext.getResources().getString(R.string.native_name) + " " + countries.getNativeName());
            mTextViewCapital.setText(mContext.getResources().getString(R.string.capital) + " " + countries.getCapital());

            String borderCountries = "";
            for (int i = 0; i < mCountriesList.size(); i++){
                borderCountries = borderCountries + " " + mCountriesList.get( i).getName();
            }

          //  mTextViewBorders.setText( borderCountries);
            }
        }

    }


