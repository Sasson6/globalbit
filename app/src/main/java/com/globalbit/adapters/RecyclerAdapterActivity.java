package com.globalbit.adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.globalbit.ICountrySelected;
import com.globalbit.R;
import com.globalbit.model.Countries;

import java.util.ArrayList;
import java.util.Objects;

public class RecyclerAdapterActivity extends RecyclerView.Adapter<RecyclerAdapterActivity.ListViewHolder> {

    private Context mContext;
    private ArrayList<Countries> mCountriesList;
    private Activity mActivity;
    private ICountrySelected mCountrySelected;

    public RecyclerAdapterActivity(Context context, ArrayList<Countries> countriesList, Activity activity, ICountrySelected aCountrySelected) {
        mContext = context;
        mCountriesList = countriesList;
        mActivity = activity;
        mCountrySelected = aCountrySelected;
    }


    @NonNull
    @Override
    public RecyclerAdapterActivity.ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.country_list_item, viewGroup, false);
        ListViewHolder viewHolder = new ListViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterActivity.ListViewHolder listViewHolder, int i) {
         listViewHolder.bindList(mCountriesList.get(i));

    }

    @Override
    public int getItemCount() {
        if (mCountriesList != null){
            return mCountriesList.size();
        }
        return 0;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTextViewLatinName;
        public TextView mTextViewNativeName;
        public ImageView mImageViewFlag;
        public TextView mTextViewCapital;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextViewLatinName = itemView.findViewById(R.id.latinName);
            mTextViewNativeName = itemView.findViewById(R.id.nativeName);
            mImageViewFlag = itemView.findViewById(R.id.imageViewFlag);
            mTextViewCapital = itemView.findViewById(R.id.capitalName);

            itemView.setOnClickListener(this);
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        public void bindList(Countries countries){
            mTextViewLatinName.setText(mContext.getResources().getString(R.string.latin_name) + " " + countries.getName());
            mTextViewNativeName.setText(mContext.getResources().getString(R.string.native_name) + " " + countries.getNativeName());
            mTextViewCapital.setText(mContext.getResources().getString(R.string.capital) + " " + countries.getCapital());

            SvgLoader.pluck()
                    .with(mActivity)
                    .load(Objects.requireNonNull(countries).getFlag(), mImageViewFlag);

            itemView.setTag(countries);
        }

        @Override
        public void onClick(View v) {
            int pos= getAdapterPosition();
            Countries selected = mCountriesList.get(pos);

            if (mCountrySelected != null){
                mCountrySelected.countrySelected( selected);
            }
        }
    }
}
