package com.globalbit;

import com.globalbit.model.Countries;

import java.util.ArrayList;

public interface IDataArrived {
    void allCountriesArrived(ArrayList<Countries> listCountries);
    void bordersArrived(ArrayList<Countries> listCountries);
}
