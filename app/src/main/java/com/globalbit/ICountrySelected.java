package com.globalbit;

import com.globalbit.model.Countries;

public interface ICountrySelected {
    void countrySelected(Countries aCountry);
}
