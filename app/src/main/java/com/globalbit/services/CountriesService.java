package com.globalbit.services;

import android.util.Log;

import com.globalbit.IDataArrived;
import com.globalbit.model.Countries;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CountriesService{

    private static final String TAG = "CountriesService";
    private static final String BASE_URL = "https://restcountries.eu/rest/v2/";
    private static ICountriesService mICountriesService = null;
    private static final String ERROR_MESSAGE = "errorMessage";



    public static void getAllCountries(final IDataArrived iDataArrived) {

        Call<List<Countries>> call = getRetrofitObject().getAllCountries();
        call.enqueue(new Callback<List<Countries>>() {
            @Override
            public void onResponse(Call<List<Countries>> call, Response<List<Countries>> response) {
                    Log.d(TAG, "all countries arrived " + response.code());
                    iDataArrived.allCountriesArrived((ArrayList<Countries>) response.body());
            }

            @Override
            public void onFailure(Call<List<Countries>> call, Throwable t) {
            }
        });
    }


    public static void getBorders(String codes, final IDataArrived iDataArrived) {

        Call<List<Countries>> call = getRetrofitObject().getAllBorders(codes);

        call.enqueue(new Callback<List<Countries>>() {
            @Override
            public void onResponse(Call<List<Countries>> call, Response<List<Countries>> response) {
                Log.d(TAG, "all boarders arrived " + response.code());
                iDataArrived.bordersArrived((ArrayList<Countries>) response.body());
            }

            @Override
            public void onFailure(Call<List<Countries>> call, Throwable t) {

            }
        });
    }

        private static ICountriesService getRetrofitObject() {
            Retrofit retrofit;

            if (mICountriesService != null) {
                return mICountriesService;
            }
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
            httpClientBuilder.addInterceptor(logging);
            OkHttpClient httpClient = httpClientBuilder.build();


            retrofit = new Retrofit.Builder()
                    .client(httpClient)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mICountriesService = retrofit.create(ICountriesService.class);
            return mICountriesService;
        }
}
