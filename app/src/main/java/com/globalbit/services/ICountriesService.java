package com.globalbit.services;

import com.globalbit.model.Countries;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ICountriesService {

    @GET("all")
    Call<List<Countries>> getAllCountries();

    @GET("alpha")
    Call<List<Countries>> getAllBorders(@Query("codes") String codeBorders);
}
