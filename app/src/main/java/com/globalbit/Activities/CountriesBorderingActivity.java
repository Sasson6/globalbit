package com.globalbit.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.globalbit.R;
import com.globalbit.adapters.RecyclerAdapterActivityBorders;
import com.globalbit.model.Countries;

import java.util.ArrayList;

public class CountriesBorderingActivity extends AppCompatActivity{

    private ArrayList<Countries> mCountriesList;
    private Countries mCountry;
    private TextView mTextViewTitleCountry;
    private TextView mTextViewTitle;
    private RecyclerView mRecyclerView;
    private RecyclerAdapterActivityBorders mRecyclerAdapterActivityBorders;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.countries_bordering);

        mCountriesList = (ArrayList<Countries>) getIntent().getSerializableExtra("countriesList");
        mCountry = (Countries)getIntent().getSerializableExtra("country");

        mTextViewTitleCountry = findViewById(R.id.textViewCountryTitle);
        mTextViewTitle = findViewById(R.id.textViewTitle);
        mRecyclerView = findViewById(R.id.recyclerViewBorders);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerAdapterActivityBorders = new RecyclerAdapterActivityBorders(getApplicationContext(), mCountriesList, CountriesBorderingActivity.this);
        mRecyclerView.setAdapter(mRecyclerAdapterActivityBorders);



        if (mCountry.getBorders().isEmpty()){
            mTextViewTitleCountry.setText(R.string.warning_message);
            mTextViewTitle.setVisibility(View.GONE);
        }else {
            mTextViewTitleCountry.setText(mCountry.getName());
        }
    }
}



