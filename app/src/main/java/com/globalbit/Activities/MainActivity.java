package com.globalbit.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.globalbit.ICountrySelected;
import com.globalbit.IDataArrived;
import com.globalbit.R;
import com.globalbit.adapters.RecyclerAdapterActivity;
import com.globalbit.model.Countries;
import com.globalbit.services.CountriesService;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IDataArrived, ICountrySelected {

    private RecyclerView mRecyclerView;
    private ArrayList<Countries> mCountries;
    private RecyclerAdapterActivity mRecyclerAdapterActivity;
    private Countries mSelectedCountry;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CountriesService.getAllCountries( this);

        mRecyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

    }

    public void updateCountriesViews(){
        mRecyclerAdapterActivity = new RecyclerAdapterActivity(getApplicationContext(), mCountries, MainActivity.this, this);
        mRecyclerView.setAdapter(mRecyclerAdapterActivity);
    }

    @Override
    public void allCountriesArrived(ArrayList<Countries> listCountries) {
        mCountries = (ArrayList<Countries>) listCountries;
        updateCountriesViews();
    }

    @Override
    public void bordersArrived(ArrayList<Countries> listCountries) {
        mCountries = (ArrayList<Countries>) listCountries;
        Intent intent = new Intent(this, CountriesBorderingActivity.class);
        intent.putExtra( "countriesList", mCountries);
        intent.putExtra( "country", mSelectedCountry);
        startActivity( intent);
    }

    @Override
    public void countrySelected(Countries aCountry) {
        mSelectedCountry = aCountry;
        String code = "";
        for (int i = 0; i < aCountry.getBorders().size(); i++){
            code = code + aCountry.getBorders().get(i) + ";";
        }

        CountriesService.getBorders(code, this);
    }
}
